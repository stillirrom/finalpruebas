package uniminuto.pruebas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class PaginaProducto {

	WebDriver webDriver;

	int SLEEP_TIME = 5000;

	String BOTON_CANTIDAD = ".//*[@id='quantity']";
	String CANTIDAD_UNO = ".//*[@id='a-popover-5']/div/div/ul/li[1]";
	String CANTIDAD_DOS = ".//*[@id='a-popover-5']/div/div/ul/li[2]";
	String BOTON_ADD_TO_CART = ".//*[@id='add-to-cart-button']";

	public PaginaProducto(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public void addToCart() throws InterruptedException {
		WebElement addToCartButton = webDriver.findElement(By.xpath(BOTON_ADD_TO_CART));
		addToCartButton.click();
		Thread.sleep(SLEEP_TIME);
	}

	public void establecerCantidad(int cantidad) throws InterruptedException {
		Select botonCantidad = new Select(webDriver.findElement(By.xpath(BOTON_CANTIDAD)));
		botonCantidad.selectByValue(Integer.toString(cantidad));
		Thread.sleep(SLEEP_TIME);
	}

	// public void setLocation(String location) throws InterruptedException {
	// WebElement locationInput =
	// webDriver.findElement(By.xpath(LOCATION_INPUT));
	// locationInput.clear();
	// locationInput.sendKeys(location);
	// Thread.sleep(SLEEP_TIME);
	// }
	//
	// public void setCheckInDate(String checkInDate) throws
	// InterruptedException {
	// WebElement checkInDateInput =
	// webDriver.findElement(By.xpath(CHECK_IN_DATE_INPUT));
	// checkInDateInput.clear();
	// checkInDateInput.sendKeys(checkInDate);
	// Thread.sleep(SLEEP_TIME);
	// }
	//
	// public void setCheckOutDate(String checkOutDate) throws
	// InterruptedException {
	// WebElement checkOutDateInput =
	// webDriver.findElement(By.xpath(CHECK_OUT_DATE_INPUT));
	// checkOutDateInput.clear();
	// checkOutDateInput.sendKeys(checkOutDate);
	// Thread.sleep(SLEEP_TIME);
	// }
	//
	// public void setAdultsQty(String adultsQty) throws InterruptedException {
	// WebElement adultsQtyInput =
	// webDriver.findElement(By.xpath(ADULTS_QTY_INPUT));
	// adultsQtyInput.clear();
	// adultsQtyInput.sendKeys(adultsQty);
	// Thread.sleep(SLEEP_TIME);
	// }
	//
	// public void setChildQty(String childQty) throws InterruptedException {
	// WebElement childQtyInput =
	// webDriver.findElement(By.xpath(ADULTS_QTY_INPUT));
	// childQtyInput.clear();
	// childQtyInput.sendKeys(childQty);
	// Thread.sleep(SLEEP_TIME);
	// }
	//
	// public void clickSearch() throws InterruptedException {
	// WebElement searchButton = webDriver.findElement(By.xpath(SEARCH_BUTTON));
	// searchButton.click();
	// Thread.sleep(SLEEP_TIME);
	// }
}
