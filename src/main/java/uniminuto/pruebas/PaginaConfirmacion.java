package uniminuto.pruebas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PaginaConfirmacion {

	WebDriver webDriver;

	int SLEEP_TIME = 5000;

	String CONFIRMACION_PRODUCTO_AGREGADO = ".//*[@id='huc-v2-order-row-confirm-text']/h1";
	String ICONO_CART = ".//*[@id='hlb-view-cart-announce']";

	public PaginaConfirmacion(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public String obtenerTextoConfirmacion() throws InterruptedException {
		String confirmacionProductoAgregado = webDriver.findElement(By.xpath(CONFIRMACION_PRODUCTO_AGREGADO)).getText();
		return confirmacionProductoAgregado;
	}

	public void clickBotonCart() throws InterruptedException {
		WebElement botonCart = webDriver.findElement(By.xpath(ICONO_CART));
		botonCart.click();
		Thread.sleep(SLEEP_TIME);
	}

}
