package uniminuto.pruebas;

import java.text.NumberFormat;
import java.text.ParseException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class PaginaCarrito {

	WebDriver webDriver;

	int SLEEP_TIME = 5000;

	String TEXTO_ELIMINAR = ".//*[@id='activeCartViewForm']/div[2]/div/div[4]/div[2]/div[1]/div/div/div[2]/div/span[1]";
	String CONFIRMACION_PRODUCTO_CARRITO = ".//*[@id='gutterCartViewForm']/div[3]/div[2]/div/div[1]";
	String TEXTO_CONFIRMACION_ELIMINACION = ".//*[@id='activeCartViewForm']/div[2]/div/div[3]/div[1]/span";
	String BOTON_CANTIDAD = ".//*[@id='activeCartViewForm']/div[2]/div/div[4]/div[2]/div[3]/div/div/span/select";
	String CANTIDAD_UNO = ".//*[@id='dropdown5_0']";
	String CANTIDAD_DOS = ".//*[@id='dropdown5_1']";
	String PRECIO_ITEM = ".//*[@id='activeCartViewForm']/div[2]/div/div[4]/div[2]/div[2]/p[1]";
	String PRECIO_TOTAL = ".//*[@id='gutterCartViewForm']/div[3]/div[2]/div/div[1]/p/span/span[2]";
	String CONTADOR_ITEMS_ICONO = ".//*[@id='nav-cart-count']";

	public PaginaCarrito(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public String obtenerTextoProductosCarrito() {
		String confirmacionProductoAgregado = webDriver.findElement(By.xpath(CONFIRMACION_PRODUCTO_CARRITO)).getText();
		return confirmacionProductoAgregado;
	}

	public void clickTextoEliminar() throws InterruptedException {
		WebElement textoEliminar = webDriver.findElement(By.xpath(TEXTO_ELIMINAR));
		textoEliminar.click();
		Thread.sleep(SLEEP_TIME);
	}

	public String obtenerTextoConfirmacionEliminacion() {
		String textoConfirmacionEliminacion = webDriver.findElement(By.xpath(TEXTO_CONFIRMACION_ELIMINACION)).getText();
		return textoConfirmacionEliminacion;
	}

	public void establecerCantidad(int cantidad) throws InterruptedException {
		Select botonCantidad = new Select(webDriver.findElement(By.xpath(BOTON_CANTIDAD)));
		botonCantidad.selectByValue(Integer.toString(cantidad));
		Thread.sleep(SLEEP_TIME);
	}
	
	public float obtenerPrecioItem() throws InterruptedException, ParseException {
		String precioItem = webDriver.findElement(By.xpath(PRECIO_ITEM)).getText();
		Number number = NumberFormat.getCurrencyInstance().parse(precioItem);
		return number.floatValue();
	}

	public float obtenerPrecioTotal() throws ParseException {
		String precioTotal = webDriver.findElement(By.xpath(PRECIO_TOTAL)).getText();
		Number number = NumberFormat.getCurrencyInstance().parse(precioTotal);
		return number.floatValue();
	}

	public int obtenerCantidadProductosIcono() {
		String contadorItemsIcono = webDriver.findElement(By.xpath(CONTADOR_ITEMS_ICONO)).getText();
		return Integer.parseInt(contadorItemsIcono);
	}
	
	
}
