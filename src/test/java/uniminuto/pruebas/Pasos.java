package uniminuto.pruebas;

import java.text.ParseException;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.TestCase;

public class Pasos extends TestCase {

	WebDriver webDriver;
	PaginaProducto paginaProducto;
	PaginaConfirmacion paginaConfirmacion;
	PaginaCarrito paginaCarrito;

	String HOME_URL = "https://www.amazon.com/After-Laughter-Paramore/dp/B07228RKVS/";
	
	String CONFIRMACION = "Added to Cart";
	String TEXTO_ITEM = "item";
	String CONFIRMACION_ELIMINACION_CARRITO = "was removed from Shopping Cart";

	@Before
	public void setUpClass() {
		webDriver = new FirefoxDriver();
		paginaProducto = new PaginaProducto(webDriver);
		paginaConfirmacion = new PaginaConfirmacion(webDriver);
		paginaCarrito = new PaginaCarrito(webDriver);
	}

	@After
	public void endProccess() {
		webDriver.close();
	}

	@Given("Estoy en la página del producto")
	public void cargarPaginaDeInicio() throws InterruptedException {
		webDriver.navigate().to(HOME_URL);
	}

	@When("Hago click sobre el boton Add To Cart")
	public void clickAgregarProductoACarrito() throws InterruptedException {
		paginaProducto.addToCart();
	}

	@Then("Se muestra una confirmacion")
	public void validarConfirmacion() throws InterruptedException {
		Assert.assertTrue(paginaConfirmacion.obtenerTextoConfirmacion().contains(CONFIRMACION));
	}

	@Then("El producto esta en el carrito")
	public void validarProductoEnElCarrito() throws InterruptedException {
		paginaConfirmacion.clickBotonCart();
		Assert.assertTrue(paginaCarrito.obtenerTextoProductosCarrito().contains(TEXTO_ITEM));
	}

	@Given("Hay un producto en el carrito")
	public void agregarProductoACarrito() throws InterruptedException {
		webDriver.navigate().to(HOME_URL);
		paginaProducto.addToCart();
	}

	@When("Hago click sobre el texto eliminar")
	public void clickTextoEliminar() throws InterruptedException {
		paginaConfirmacion.clickBotonCart();
		paginaCarrito.clickTextoEliminar();
	}

	@Then("Se mostrara la confirmacion de eliminacion")
	public void validarConfirmacionEliminacion() {
		Assert.assertTrue(
				paginaCarrito.obtenerTextoConfirmacionEliminacion().contains(CONFIRMACION_ELIMINACION_CARRITO));
	}

	@Given("Selecciono Cantidad: (\\d+)")
	public void seleccionarCantidad(int cantidad) throws InterruptedException {
		paginaProducto.establecerCantidad(cantidad);
	}

	@Then("La cantidad es: (\\d+)")
	public void validarCantidad(int cantidad) {
		Assert.assertTrue(paginaCarrito.obtenerTextoProductosCarrito().contains(Integer.toString(cantidad)));
	}

	@Given("Hay un producto con cantidad: (\\d+) en el carrito")
	public void agregarCantidadDeProductoACarrito(int cantidad) throws InterruptedException {
		webDriver.navigate().to(HOME_URL);
		paginaProducto.establecerCantidad(cantidad);
		paginaProducto.addToCart();
	}

	@When("Cambio la cantidad a: (\\d+)")
	public void cambiarCantidadDeProducto(int cantidad) throws InterruptedException {
		paginaConfirmacion.clickBotonCart();
		paginaCarrito.establecerCantidad(cantidad);
	}
	
	@Then("El precio se multiplica por: (\\d+)")
	public void verificarPrecioMultiplicado(int cantidad) throws InterruptedException, ParseException{
		paginaConfirmacion.clickBotonCart();
		System.out.println(paginaCarrito.obtenerPrecioItem());
		System.out.println(paginaCarrito.obtenerPrecioTotal());
		Assert.assertEquals(paginaCarrito.obtenerPrecioItem() * cantidad, paginaCarrito.obtenerPrecioTotal(), 0.1);
	}
	
	@Then("El contador de items en el icono del carrito sera: (\\d+)")
	public void verificarIconoContadorProductos(int cantidad){
		Assert.assertEquals(paginaCarrito.obtenerCantidadProductosIcono(), cantidad);
	}

}
