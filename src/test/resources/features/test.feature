Feature: Carrito de Compras

  Scenario: Agregar Producto
    Given Estoy en la página del producto
    When Hago click sobre el boton Add To Cart
    Then Se muestra una confirmacion
    And El producto esta en el carrito

  Scenario: Eliminar Producto
    Given Hay un producto en el carrito
    When Hago click sobre el texto eliminar
    Then Se mostrara la confirmacion de eliminacion

  Scenario: Agregar x cantidad de un Producto
    Given Estoy en la página del producto
    And Selecciono Cantidad: 2
    When Hago click sobre el boton Add To Cart
    Then El producto esta en el carrito
    And La cantidad es: 2

  Scenario: Aumentar x cantidad de un producto
    Given Hay un producto con cantidad: 4 en el carrito
    When Cambio la cantidad a: 6
    Then La cantidad es: 6
    
  Scenario: Eliminar x cantidad de un Producto
    Given Hay un producto con cantidad: 3 en el carrito
    When Cambio la cantidad a: 1
    Then La cantidad es: 1

  Scenario: Multiplicar los precios por la cantidad
    When Hay un producto con cantidad: 5 en el carrito
    Then El precio se multiplica por: 5

  Scenario: El contador de items en el icono del carrito corresponde a la cantidad de items
    When Hay un producto con cantidad: 7 en el carrito
    Then El contador de items en el icono del carrito sera: 7